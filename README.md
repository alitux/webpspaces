# WebpSpaces

Utility to convert JPG and PNG images to webp and upload them DigitalOcean Spaces.

## Installation

```bash
pip install git+https://gitlab.com/alitux/webspaces
```

## Usage
### Convert all images (JPG&PNG --> webp), upload to DigitalOcean Spaces and delete all images (JPG, PNG and webp)

```bash
webpsp -a -da --region REGION -e https://yourspace.sfo3.digitaloceanspaces.com -k yourkey -s yoursecret -f yourspace
```
### Only convert images to webp

```bash
webpsp -a
```

## Roadmap
- [] Download all images from a web, convert images and upload to DigitalOcean Spaces

- [] Generate csv with images and links


## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
