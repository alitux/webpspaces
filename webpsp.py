#!/usr/bin/env python
# -*- coding: utf-8 -*-

import boto3
import argparse
import os
from pathlib import Path
from PIL import Image


parser = argparse.ArgumentParser()

# Arguments
parser.add_argument("-r", "--region", help="Region to upload content. Example: sfo3")
parser.add_argument("-e", "--endpoint", help="Endpoint to upload content. Example: https://yourendpoint.sfo3.digitaloceanspaces.com")
parser.add_argument("-k", "--key", help="Your API key id")
parser.add_argument("-s", "--secret", help="Your API Secret key")
parser.add_argument("-f", "--folder", help="Folder in DigitalOcean Space. Example: YourFolder")
parser.add_argument("-a", "--all", help="Convert all images(PNG and JPG)", action="store_true")
parser.add_argument("-da", "--delete-all", help="Delete all images(JPG, PNG and webp) after upload", action="store_true")

args = parser.parse_args()

def convert_to_webp(source):
    """Convert image to webp.

    Args:
        source (pathlib.Path): Path to source image

    Returns:
        pathlib.Path: path to new image
    """
    destination = source.with_suffix(".webp")

    image = Image.open(source)  # Open image
    image.save(destination, format="webp")  # Convert image to webp

    return destination


def file_to_space (region,endpoint,key_id,access_key,file_object,folder_name):
    """Upload Objects to DigitalOcean Spaces.

    """

    session = boto3.session.Session()

    client = session.client('s3',
                            region_name=region,
                            endpoint_url=endpoint,
                            aws_access_key_id=key_id,
                            aws_secret_access_key=access_key)

    client.upload_file(file_object,  # Path to local file
                       folder_name,  # Name of Space
                       file_object,ExtraArgs={'ACL':'public-read'})  # Name for remote file

    print (f"{file_object} --> {endpoint}/{folder_name}/{file_object} [OK]")

def del_image(image):
    """Remove images
    """
    try:
        os.remove(image)
        print("% s removed successfully" % image)
    except OSError as error:
        print(error)
        print("File path can not be removed")

def main():
    if args.all:
        print ("- [CONVERT] images to webp")
        images_list=[]
        images_total_size_jpg=0
        images_total_size_webp=0

        paths_png = Path(".").glob("**/*.png")
        for path in paths_png:
            webp_path = convert_to_webp(path)
            images_total_size_jpg += Path(path).stat().st_size
            images_list.append(webp_path)
            images_total_size_webp+= Path(webp_path).stat().st_size
            print (str(webp_path) + " [OK]")

        paths_jpg = Path(".").glob("**/*.jpg")
        for path in paths_jpg:
            webp_path = convert_to_webp(path)
            images_total_size_jpg += Path(path).stat().st_size
            images_list.append(webp_path)
            images_total_size_webp+= Path(webp_path).stat().st_size
            print(str(webp_path) + " [OK]")
        print (f"Size Reduction: JPG/PNG: {round((images_total_size_jpg/1024),1)} Kb-->WEBP: {round((images_total_size_webp/1024),1)} Kb ({round((images_total_size_webp*100/images_total_size_jpg),2)})%")

        if args.all and args.region and args.endpoint and args.key and args.secret and args.folder:
            print (f"[UPLOAD] images to {args.endpoint}")
            for image in images_list:
                file_to_space(args.region,args.endpoint,args.key,args.secret,str(image),args.folder)

        if args.delete_all:
            print (f"- [REMOVE] JPG/PNG & webp images")
            for image in images_list:
                del_image(image)
            for image in Path(".").glob("**/*.png"):
                del_image(image)
            for image in Path(".").glob("**/*.jpg"):
                del_image(image)
main()
